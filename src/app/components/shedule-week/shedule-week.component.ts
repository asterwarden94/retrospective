import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppService} from '../../services/AppService.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-shedule-week',
  templateUrl: './shedule-week.component.html',
  styleUrls: ['./shedule-week.component.scss'],
  providers: [AppService]
})
export class SheduleWeekComponent implements OnInit, OnDestroy {

  private memosSubscription: Subscription;
  public memosList: Array<any>;

  constructor(
    private appService: AppService
  ) { }

  ngOnInit(): void {
    this.memosSubscription = this.appService.getData('memos').subscribe(item => {
      // if (!isPresent) {
      //   return;
      // }
      this.memosList = Object.values(item).sort((a, b) => a.createdAt > b.createdAt ? 1 : -1);
    });
  }

  ngOnDestroy(): void {
    this.memosSubscription.unsubscribe();
  }

}
