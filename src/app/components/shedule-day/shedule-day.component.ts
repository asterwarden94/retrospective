import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-shedule-day',
  templateUrl: './shedule-day.component.html',
  styleUrls: ['./shedule-day.component.scss']
})
export class SheduleDayComponent implements OnInit {

  @Input() memoValue;

  constructor() { }

  ngOnInit(): void {
  }

}
