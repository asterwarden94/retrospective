import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SheduleWeekComponent } from './components/shedule-week/shedule-week.component';
import { SheduleDayComponent } from './components/shedule-day/shedule-day.component';
import {HttpClientModule} from '@angular/common/http';
import {PipeModuleModule} from './modules/pipe-module/pipe-module.module';
import {AsyncPipe} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SheduleWeekComponent,
    SheduleDayComponent,
  ],
  imports: [
    BrowserModule,
    PipeModuleModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
