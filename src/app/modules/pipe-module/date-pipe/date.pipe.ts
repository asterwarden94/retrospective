import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'datePipe'
})
export class DatePipe implements PipeTransform {
  transform(value: string, args?: any): string {
    return moment(Number(Date.parse(value.replace(' ', '')))).format('MMMM DD YYYY, hh:mm');
  }
}
